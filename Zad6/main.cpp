#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <limits>
#include <cmath>

struct InLine {
    size_t c = 0;
    std::vector<double> v;

    double dist(const InLine& e) const {
        double ret = 0.0;
        for(size_t i = 0; i < v.size(); ++i) {
            ret += fabs(v[i] - e.v[i]);
        }
        return ret;
    }
};

int main(int argc, char* argv[]) {

    std::cout << " Klasyfikator dzialajacy wg reguly 1-NS\n";

    if(argc < 4) {
        std::cout << argv[0] << " <Nazwa pliku uczacego> <Nazwa pliku testujacego> <Nazwa pliku wynikowego>\n";
        return 0;
    }

    std::ifstream ucz(argv[1]);
    std::ifstream tst(argv[2]);
    std::ofstream out(argv[3]);

    size_t classes = 0;
    size_t features = 0;
    size_t lines = 0;
    ucz >> classes >> features >> lines;
    InLine reader;
    std::vector<InLine> learnData;
    learnData.reserve(lines);
    std::vector<double> meanVals(features, 0.0);

    for(size_t i = 0; i < lines; ++i) {
        ucz >> reader.c;
        reader.v.resize(features);
        for(size_t j = 0; j < features; ++j) {
            ucz >> reader.v[j];
            meanVals[j] += reader.v[j];
        }
        learnData.emplace_back(reader);
    }

    for(size_t i = 0; i < features; ++i) {
        meanVals[i] /= lines;
    }

    std::vector<double> stdDev(features, 0.0);

    for(size_t i = 0; i < lines; ++i) {
        for(size_t j = 0; j < features; ++j) {
            stdDev[j] += ((learnData[i].v[j] - meanVals[j]) * (learnData[i].v[j] - meanVals[j])) / (lines - 1);
        }
    }

    for(size_t i = 0; i < features; ++i) {
        stdDev[i] = std::sqrt(stdDev[i]);
    }

    // Standarize control set
    for(auto& e : learnData) {
        for(size_t i = 0; i < features; ++i) {
            e.v[i] = (e.v[i] - meanVals[i]) / stdDev[i];
        }
    }

    std::vector<InLine> testData;
    tst >> classes >> features >> lines;
    testData.reserve(lines);

    // Standarize test data
    for(size_t i = 0; i < lines; ++i) {
        tst >> reader.c;
        reader.v.resize(features);
        for(size_t j = 0; j < features; ++j) {
            tst >> reader.v[j];
            reader.v[j] = (reader.v[j] - meanVals[j]) / stdDev[j];
        }
        testData.emplace_back(reader);
    }

    out << "\n Wyniki testu:\n";
    out << " Nr obj, Klasa faktyczna, Klasa przypisana\n";

    size_t c = 0;
    double minDist = 0;

    std::vector<std::vector<size_t>> hitMatrix(classes, std::vector<size_t>(classes,0));

    for(size_t i = 0; i < lines; ++i) {
        minDist = std::numeric_limits<double>::max();
        c = 0;
        for (const auto &j : learnData) {
            double dist = j.dist(testData[i]);
            if(dist < minDist) {
                c = j.c;
                minDist = dist;
            }
        }

        out << std::setw(7) << i << std::setw(17) << testData[i].c << std::setw(18) << c << std::endl;
        ++hitMatrix[testData[i].c - 1][c - 1];
    }

    out << "\n Czestosci przeklaman:\n";
    std::vector<size_t> sums(classes, 0);
    out << std::setw(8) << ' ';
    for(size_t i = 0; i < classes; ++i) {
        out << std::setw(8) << i + 1;
    }

    out << '\n';

    size_t misses = 0;
    for(size_t i = 0; i < hitMatrix.size(); ++i) {
        out << std::setw(8) << i + 1;
        sums[i] = 0;
        for(size_t j = 0; j < hitMatrix[i].size(); ++j) {
            sums[i] += hitMatrix[i][j];
            out << std::setw(8) << hitMatrix[i][j];
            if(i != j) {
                misses += hitMatrix[i][j];
            }
        }
        out << '\n';
    }

    out << "\n Prawdopodobienstwa a priori:\n";
    out << std::setw(8) << ' ';
    for(size_t i = 0; i < classes; ++i) {
        out << std::setw(8) << i + 1;
    }

    out << '\n';

    for(size_t i = 0; i < hitMatrix.size(); ++i) {
        out << std::setw(8) << i + 1;
        for(size_t j = 0; j < hitMatrix[i].size(); ++j) {
            out << std::setw(8) << std::fixed << std::setprecision(4) << double(hitMatrix[i][j]) / sums[i];
        }
        out << '\n';
    }

    for(size_t i = 0; i < hitMatrix.size(); ++i) {
        sums[i] = 0;
        for(size_t j = 0; j < hitMatrix[i].size(); ++j) {
            sums[i] += hitMatrix[j][i];
        }
    }

    out << "\n Prawdopodobienstwa a posteri:\n";
    out << std::setw(8) << ' ';
    for(size_t i = 0; i < classes; ++i) {
        out << std::setw(8) << i + 1;
    }

    out << '\n';

    for(size_t i = 0; i < hitMatrix.size(); ++i) {
        out << std::setw(8) << i + 1;
        for(size_t j = 0; j < hitMatrix[i].size(); ++j) {
            out << std::setw(8) << std::fixed << std::setprecision(4) << double(hitMatrix[i][j]) / sums[i];
        }
        out << '\n';
    }

    std::cout << "\n\n Odsetek mylnych decyzji: " << 100.0 * misses / lines << "%\n";
    out << "\n Odsetek mylnych decyzji: " << 100.0 * misses / lines << "%\n";

    return 0;
}