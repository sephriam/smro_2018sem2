#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <limits>
#include <cmath>

struct InLine {
    std::vector<double> v;

    double dist(const InLine& e) const {
        double ret = 0.0;
        for(size_t i = 0; i < v.size(); ++i) {
            if(v[i] > e.v[i]) {
                ret += (v[i] - e.v[i]);
            } else {
                ret -= (v[i] - e.v[i]);
            }
        }
        return ret;
    }
};

int main(int argc, char* argv[]) {

    if(argc < 3) {
        std::cout << argv[0] << " <Nazwa pliku uczacego> <Nazwa pliku wynikowego>\n";
        return 0;
    }

    std::ifstream ucz(argv[1]);
    std::ofstream out(argv[2]);

    out << "\n Program do wyznaczania obszarow klas.\n";

    size_t classes = 0;
    size_t features = 0;
    size_t lines = 0;
    ucz >> classes >> features >> lines;
    InLine reader;
    std::vector<std::vector<InLine>> learnData(classes);
    learnData.reserve(lines);
    std::vector<double> meanVals(features, 0.0);
    size_t c;

    for(size_t i = 0; i < lines; ++i) {
        ucz >> c;
        reader.v.resize(features);
        for(size_t j = 0; j < features; ++j) {
            ucz >> reader.v[j];
            meanVals[j] += reader.v[j];
        }
        learnData[c - 1].emplace_back(reader);
    }

    for(size_t i = 0; i < features; ++i) {
        meanVals[i] /= lines;
    }

    std::vector<double> stdDev(features, 0.0);

    for(size_t k = 0; k < classes; ++k) {
        for (auto &line : learnData[k]) {
            for (size_t j = 0; j < features; ++j) {
                stdDev[j] += ((line.v[j] - meanVals[j]) * (line.v[j] - meanVals[j])) / (lines - 1);
            }
        }
    }

    for(size_t i = 0; i < features; ++i) {
        stdDev[i] = std::sqrt(stdDev[i]);
    }

    // Standarize control set
    for(size_t k = 0; k < classes; ++k) {
        for (auto &line : learnData[k]) {
            for (size_t j = 0; j < features; ++j) {
                line.v[j] = (line.v[j] - meanVals[j]) / stdDev[j];
            }
        }
    }

    std::vector<double> maxDists(classes, 0.0);

    for(size_t i = 0; i < classes; ++i) {
        for(size_t j = 0; j < learnData[i].size(); ++j) {
            double closestNeighbour = std::numeric_limits<double>::max();
            // nearest neighbour
            for(size_t k = 0; k < learnData[i].size(); ++k) {
                if(j == k) {
                    continue;
                }
                double dist = learnData[i][j].dist(learnData[i][k]);
                if(dist < closestNeighbour) {
                    closestNeighbour = dist;
                }
            }

            if(closestNeighbour > maxDists[i]) {
                maxDists[i] = closestNeighbour;
            }
        }
    }

    size_t cnt = 1;
    out << " Obiekt klasa   A1 A2 A3\n";

    size_t overlapping = 0;

    std::vector<unsigned char> A(classes, 0);
    for(size_t i = 0; i < classes; ++i) {
        for(size_t j = 0; j < learnData[i].size(); ++j) {
            A.clear();
            A.resize(classes, 0);

            bool canOverlap = true;
            for(size_t k = 0; k < classes; ++k) {
                if(k == i) {
                    A[k] = 1;
                    continue;
                }

                for (size_t l = 0; l < learnData[i].size(); ++l) {
                    if(learnData[k][l].dist(learnData[i][j]) < maxDists[k]) {
                        if(canOverlap) {
                            ++overlapping;
                            canOverlap = false;
                        }
                        A[k] = 1;
                        break;
                    }
                }
            }

            out << std::setw(7) << cnt++ << std::setw(6) << i + 1 << "  ";
            for(const auto& v : A) {
                out << std::setw(3) << int(v);
            }
            out << '\n';
        }
    }

    out << "\n Frakcja pokrywania klas: " << std::fixed << std::setprecision(4) << double(overlapping) / lines << '\n';

    return 0;
}