#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <limits>
#include <cmath>
#include <set>

struct InLine {
    size_t c = 0;
    std::vector<double> v;

    double dist(const InLine& e) const {
        double ret = 0.0;
        for(size_t i = 0; i < v.size(); ++i) {
            ret += fabs(v[i] - e.v[i]);
        }
        return ret;
    }
};

bool isAnythingInsideBall(const InLine& x1, const InLine& x2, const std::vector<InLine>& elements) {

    InLine center;
    for(size_t i = 0; i < x1.v.size(); ++i) {
        center.v.push_back((x1.v[i] + x2.v[i]) / 2.0);
    }

    double radius = x1.dist(x2) / 2.0;

    for(const auto& e : elements) {
        if(&e == &x1 || &e == &x2) {
            continue;
        } else if(center.dist(e) < radius) {
            return true;
        }
    }

    return false;
}

int main(int argc, char* argv[]) {

    if(argc < 3) {
        std::cout << argv[0] << " <Nazwa pliku uczacego> <Nazwa pliku wynikowego>\n";
        return 0;
    }

    std::ifstream ucz(argv[1]);
    std::ofstream out(argv[2]);

    out << "\n Program do wyznaczania obszarow klas.\n";

    size_t classes = 0;
    size_t features = 0;
    size_t lines = 0;
    ucz >> classes >> features >> lines;
    InLine reader;
    std::vector<InLine> learnData;
    learnData.reserve(lines);
    std::vector<double> meanVals(features, 0.0);

    for(size_t i = 0; i < lines; ++i) {
        ucz >> reader.c;
        reader.v.resize(features);
        for(size_t j = 0; j < features; ++j) {
            ucz >> reader.v[j];
            meanVals[j] += reader.v[j];
        }
        learnData.emplace_back(reader);
    }

    for(size_t i = 0; i < features; ++i) {
        meanVals[i] /= lines;
    }

    std::vector<double> stdDev(features, 0.0);

    for (auto &line : learnData) {
        for (size_t j = 0; j < features; ++j) {
            stdDev[j] += ((line.v[j] - meanVals[j]) * (line.v[j] - meanVals[j])) / (lines - 1);
        }
    }


    for(size_t i = 0; i < features; ++i) {
        stdDev[i] = std::sqrt(stdDev[i]);
    }

    // Standarize control set
    for (auto &line : learnData) {
        for (size_t j = 0; j < features; ++j) {
            line.v[j] = (line.v[j] - meanVals[j]) / stdDev[j];
        }
    }

    std::set<size_t> memNums;

    for(size_t i = 0; i < learnData.size(); ++i) {
        for(size_t j = 0; j < learnData.size(); ++j) {
            if(memNums.find(i) != memNums.end()) {
                continue;
            }

            if(learnData[i].c != learnData[j].c && !isAnythingInsideBall(learnData[i], learnData[j], learnData)) {
                memNums.insert(i);
            }
        }
    }

    out << " " << classes << " " << features << " " << memNums.size() << '\n';

    for(const auto& i : memNums) {
        out << std::setw(3) << learnData[i].c;
        for(const auto& v : learnData[i].v) {
            out << std::setw(10) << std::fixed << std::setprecision(4) << v;
        }
        out << '\n';
    }

    for(const auto& i : memNums) {
        out << i << '\n';
    }

    return 0;
}