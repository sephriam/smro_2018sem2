#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>

void work(const std::string &filePath, size_t lines, size_t &features, size_t c1, size_t c2,
          std::vector<double> &v, size_t &corrections, size_t maxCorrections, size_t &hits,
          size_t &okLines) {

    hits = 0;
    lines = 0;
    okLines = 0;
    std::fstream file(filePath);

    file >> features >> features >> lines;
    v.resize(features + 1, 0.0);
    std::vector<double> f1(features + 1);
    std::vector<double> f2(features);

    size_t c;
    for (size_t l = 0; l < lines; ++l) {

        file >> c;
        if (c == c1 || c == c2) {
            ++okLines;
        }

        f1.clear();
        f2.clear();
        for (size_t i = 0; i < features; ++i) {

            double val;
            file >> val;
            f2.push_back(val);
            if (c == c2) {
                //val = -val;
            }
            f1.push_back(val);
        }
        f1.push_back(c == c1 ? 1.0 : -1.0);

        if (c != c1 && c != c2) {
            continue;
        }

        // used for error correction
        double y1 = 0.0;
        for (size_t i = 0; i < features + 1; ++i) {
            y1 += f1[i] * v[i];
        }

        // used for qualification
        double y2 = 0.0;
        for (size_t i = 0; i < features; ++i) {
            y2 += f2[i] * v[i];
        }

        y2 += v[features];

        if ((c == c2 && y2 < 0) || (c == c1 && y2 >= 0)) {
            ++hits;
        }

        if (y1 <= 0 && corrections < maxCorrections) {
            ++corrections;
            std::cout << okLines << " ";
            for (size_t i = 0; i < features + 1; ++i) {
                v[i] += f1[i];
                std::cout << v[i] << " ";
            }
            std::cout << '\n';
        }
    }
}

int main(int argc, char *argv[]) {

    std::cout << '\n' << std::setw(20) << "Algorytm korekcji bledow.\n";
    std::cout << '\n' << std::setw(20) << "Zbior uczacy: ";
    std::string filePath = "/media/wojtowic/D/notatki/mgr/Sem2/Zaoczne2018egze/ucz3.txt";
    //std::cin >> filePath;

    std::cout << '\n' << std::setw(20) << "Zbior wynikowy: ";
    std::string outFilePath = "out.txt";
    //std::cin >> outFilePath;

    std::cout << std::setw(20) << "Podaj pare klas: ";
    size_t c1 = 1;
    size_t c2 = 2;
    //std::cin >> c1 >> c2;

    std::cout << std::setw(20) << "Maksymalna liczba korekcji: ";
    size_t maxCorrections = 5555;
    //std::cin >> maxCorrections;

    size_t features = 0;
    size_t lines = 1;
    size_t corrections = 0;
    size_t hits = 0;
    size_t okLines = 0;

    std::vector<double> v(10, 0.0);

    while (hits != lines && corrections < maxCorrections) {
        work(filePath, lines, features, c1, c2, v, corrections, maxCorrections, hits, okLines);
    }

    std::ofstream of(outFilePath);
    of << std::fixed;
    of << std::setw(20) << "\n Algorytm korekcji bledow.\n";
    of << std::setw(20) << "\n Poprawnych decyzji: " << hits << " na " << okLines << "; liczba korekcji: "
       << corrections
       << '\n';
    of << std::setw(16) << "\n Najlepsze wagi:\n";
    for (size_t i = 0; i < features + 1; ++i) {
        of << std::setw(4) << i + 1;
        of << std::setw(12) << std::setprecision(4) << double(v[i]) << '\n';
    }

    return 0;
}