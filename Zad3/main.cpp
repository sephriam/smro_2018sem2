#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <iomanip>

int main(int argc, char *argv[]) {

    std::cout << '\n' << std::setw(20) << "Algorytm korekcji bledow.\n";
    std::cout << '\n' << std::setw(20) << "Zbior uczacy: ";
    std::string filePath = "/media/wojtowic/D/notatki/mgr/Sem2/Zaoczne2018egze/ucz.txt";
    std::cin >> filePath;

    std::cout << '\n' << std::setw(20) << "Zbior wynikowy: ";
    std::string outFilePath = "out.txt";
    std::cin >> outFilePath;

    std::cout << std::setw(20) << "Podaj pare klas: ";
    size_t c1 = 1;
    size_t c2 = 2;
    std::cin >> c1 >> c2;

    std::cout << std::setw(20) << "Maksymalna liczba korekcji: ";
    size_t maxCorrections = 1000;
    std::cin >> maxCorrections;

    std::ifstream file(filePath);
    size_t features = 0;
    size_t lines = 0;
    file >> features >> features >> lines;
    std::vector<std::vector<double>> Y;

    size_t okLines = 0;

    for (size_t i = 0; i < lines; ++i) {
        size_t c;
        file >> c;

        if (c == c1 || c == c2) {
            ++okLines;
            Y.emplace_back(std::vector<double>());
        }

        for (size_t j = 0; j < features; ++j) {
            double value;
            file >> value;
            if(c == c2) {
                value = -value;
            }

            if (c == c1 || c == c2) {
                Y.back().push_back(value);
            }
        }

        if (c == c1 || c == c2) {
            Y.back().push_back(c == c1 ? 1.0 : -1.0);
        }
    }

    std::vector<std::vector<double>> v(okLines, std::vector<double>(features + 1, 0.0));
    size_t corrections = 0;
    size_t hits = 0;
    size_t maxHits = 0;
    std::vector<double> bestValues(features + 1, 0.0);
    double gx = 0.0;

    while (corrections < maxCorrections && hits != okLines) {
        for (size_t i = 0; i < okLines; ++i) {
            double vy = 0;
            for (int j = 0; j <= features; j++) {
                vy += Y[i][j] * v[(i - 1 + okLines) % okLines][j];
            }

            for (int j = 0; j <= features; j++) {
                if (vy <= 0) {
                    v[i][j] = Y[i][j] + v[(i - 1 + okLines) % okLines][j];
                } else {
                    v[i][j] = v[(i - 1 + okLines) % okLines][j];
                }
            }

            hits = 0;
            for (int a = 0; a < okLines; a++) {
                gx = 0.0;
                for (int b = 0; b < features; b++) {
                    if(Y[a].back() == -1.0) {
                        gx += v[i][b] * -Y[a][b];
                    } else {
                        gx += v[i][b] * Y[a][b];
                    }
                }
                gx += v[i][features];
                if ((gx < 0 && Y[a].back() == -1.0) ||
                    (gx > 0 && Y[a].back() == 1.0))
                    hits++;
            }
            if (hits > maxHits) {
                maxHits = hits;
                bestValues = v[i];
            }
        }
        ++corrections;
    }

    std::ofstream of(outFilePath);
    of << std::fixed;
    of << std::setw(20) << "\n Algorytm korekcji bledow.\n";
    of << std::setw(20) << "\n Poprawnych decyzji: " << maxHits << " na " << okLines << "; liczba korekcji: "
       << corrections
       << '\n';
    of << std::setw(16) << "\n Najlepsze wagi:\n";
    for (size_t i = 0; i < bestValues.size(); ++i) {
        of << std::setw(4) << i + 1;
        of << std::setw(12) << std::setprecision(4) << bestValues[i] << '\n';
    }

    return 0;
}