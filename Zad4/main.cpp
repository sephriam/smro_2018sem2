#include <iostream>
#include <vector>
#include <limits>
#include <fstream>
#include <iomanip>

// This programe work only for 1D data - class and single feature

struct InLine {
    size_t c = 0;
    double value = 0.0;
};

double calcCOG(const std::vector<InLine> &data, size_t c) {

    size_t count = 0;
    double sum = 0.0;
    for (const auto &v : data) {
        if (v.c == c) {
            sum += v.value;
            ++count;
        }
    }

    sum /= double(count);

    return sum;
}

size_t findMax(const std::vector<InLine>& data, size_t c) {

    size_t index = 0;
    double max = std::numeric_limits<double>::min();
    for (size_t i = 0; i < data.size(); ++i) {
        if(data[i].c == c && data[i].value > max) {
            index = i;
            max = data[i].value;
        }
    }

    return index;
}

size_t findMin(const std::vector<InLine>& data, size_t c) {

    size_t index = 0;
    double min = std::numeric_limits<double>::max();
    for (size_t i = 0; i < data.size(); ++i) {
        if(data[i].c == c && data[i].value < min) {
            index = i;
            min = data[i].value;
        }
    }

    return index;
}

int main(int argc, char* argv[]) {

    if(argc < 2) {
        std::cout << argv[0] << " <input file path>\n";
        return 0;
    }

    std::ifstream file(argv[1]);

    size_t classes = 0;
    size_t features = 0;
    size_t lines = 0;
    file >> classes >> features >> lines;
    std::vector<InLine> data;

    for (size_t i = 0; i < lines; ++i) {
        InLine v;
        file >> v.c;
        file >> v.value;
        data.emplace_back(v);
    }

    double hp = calcCOG(data, 2);

    std::cout << "\n Edytowanie zbiorow dla liniowej rozdzielnosci.\n\n";
    size_t iteration = 0;

    std::vector<std::pair<size_t, size_t>> deleted;

    while(true) {
        ++iteration;

        size_t aInd = findMax(data, 1);
        size_t bInd = findMin(data, 2);

        std::cout << "\n Przebieg " << iteration << ":  Skrajne obiekty:  " << aInd + 1 << " oraz " << bInd + 1 << '\n';


        if(data[aInd].value < hp && data[bInd].value > hp) {
            break;
        }

        size_t l1 = 0;
        size_t l2 = 0;

        for(const auto& v : data) {
            if(v.value <= data[aInd].value && v.value >= data[bInd].value) {

                if(v.c == 1) {
                    ++l1;
                } else {
                    ++l2;
                }
            }
        }

        std::cout << " z klas 1 i 2 odpowiednio\n";
        std::cout << " l1 = " << l1 << "  l2 = " << l2 << '\n';

        if(l1 > l2) {
            data[bInd].c = std::numeric_limits<size_t>::max();
            std::cout << " Usuwamy obiekt nr    " << bInd + 1 << " z klasy  2\n";
            deleted.emplace_back(bInd+1, 2);
        } else if(l1 < l2) {
            data[aInd].c = std::numeric_limits<size_t>::max();
            std::cout << " Usuwamy obiekt nr    " << aInd + 1 << " z klasy  1\n";
            deleted.emplace_back(aInd+1, 1);
        } else if(l1 == l2 && l1 != 0) {
            data[aInd].c = std::numeric_limits<size_t>::max();
            data[bInd].c = std::numeric_limits<size_t>::max();
            std::cout << " Usuwamy obiekt nr    " << aInd + 1 << " z klasy  1\n";
            std::cout << " Usuwamy obiekt nr    " << bInd + 1 << " z klasy  2\n";
            deleted.emplace_back(bInd+1, 2);
            deleted.emplace_back(aInd+1, 1);
        } else {
            std::cout << " Nic nie usuwamy. Sukces.\n";
            break;
        }
    }

    std::cout << "\n Usuniete obiekty to:\n";
    std::cout << " Obiekt nr   klasa\n";
    for(const auto& p : deleted) {
        std::cout << std::setw(10) << p.first << std::setw(8) << p.second << '\n';
    }

    std::cout << "\n Mylnie klasyfikowane obiekty z wybranej pary klas;\n";
    std::cout << " Obiekt nr  Klasa faktyczna  Klasa przypisana\n";
    for(const auto& p : deleted) {
        std::cout << std::setw(10) << p.first << std::setw(17) << p.second << std::setw(18) << (p.second == 1 ? 2 : 1) << '\n';
    }

    return 0;
}