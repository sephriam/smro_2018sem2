#include <iostream>
#include <fstream>
#include <vector>
#include <iomanip>
#include <limits>
#include <cmath>
#include <set>

struct InLine {
    size_t c = 0;
    std::vector<double> v;

    double dist(const InLine &e) const {
        double ret = 0.0;
        for (size_t i = 0; i < v.size(); ++i) {
            ret += fabs(v[i] - e.v[i]);
        }
        return ret;
    }

    InLine operator+(const InLine &e) const {
        InLine ret;
        for (size_t i = 0; i < v.size(); ++i) {
            ret.v.push_back(v[i] + e.v[i]);
        }
        return ret;
    }

    InLine operator-(const InLine &e) const {
        InLine ret;
        for (size_t i = 0; i < v.size(); ++i) {
            ret.v.push_back(v[i] - e.v[i]);
        }
        return ret;
    }

    InLine &operator-=(const InLine &e) {
        for (size_t i = 0; i < v.size(); ++i) {
            v[i] -= e.v[i];
        }
        return *this;
    }

    double operator*(const InLine &e) const {
        double ret = 0.0;
        for (size_t i = 0; i < v.size(); ++i) {
            ret += v[i] * e.v[i];
        }
        return ret;
    }

    InLine &operator*(double val) {
        for (size_t i = 0; i < v.size(); ++i) {
            v[i] *= val;
        }
        return *this;
    }
};

int main(int argc, char *argv[]) {

    if (argc < 3) {
        std::cout << argv[0] << " <Nazwa pliku uczacego> <Nazwa pliku wynikowego>\n";
        return 0;
    }

    std::ifstream ucz(argv[1]);
    std::ofstream out(argv[2]);

    std::cout << " Numery rozdielanych klas? ";
    size_t c1 = 1;
    size_t c2 = 2;
    std::cin >> c1 >> c2;

    size_t classes = 0;
    size_t features = 0;
    size_t lines = 0;
    ucz >> classes >> features >> lines;
    InLine reader;
    std::vector<InLine> learnData(lines);
    std::vector<double> meanVals(features, 0.0);
    size_t incorrect = 0;
    size_t c1ind = 0;
    size_t c2ind = 1;

    for (size_t i = 0; i < lines; ++i) {
        ucz >> reader.c;
        reader.v.resize(features);
        for (size_t j = 0; j < features; ++j) {
            ucz >> reader.v[j];
            meanVals[j] += reader.v[j];
        }

        if (reader.c == c1) {
            learnData[c1ind] = reader;
            c1ind += 2;
        } else if (reader.c == c2) {
            learnData[c2ind] = reader;
            c2ind += 2;
        } else {
            ++incorrect;
        }
    }

    lines -= incorrect;
    learnData.resize(lines - incorrect);

    for (size_t i = 0; i < features; ++i) {
        meanVals[i] /= lines;
    }

    std::vector<double> stdDev(features, 0.0);

    for (auto &line : learnData) {
        for (size_t j = 0; j < features; ++j) {
            stdDev[j] += ((line.v[j] - meanVals[j]) * (line.v[j] - meanVals[j])) / (lines - 1);
        }
    }


    for (size_t i = 0; i < features; ++i) {
        stdDev[i] = std::sqrt(stdDev[i]);
    }

    // Standarize control set
    for (auto &line : learnData) {
        for (size_t j = 0; j < features; ++j) {
            //line.v[j] = (line.v[j] - meanVals[j]) / stdDev[j];
        }
    }

    out << "\n Zbior uczacy uporzadkowany:\n";
    for (const auto &e : learnData) {
        out << std::setw(3) << e.c;
        for (const auto &v : e.v) {
            out << std::fixed << std::setprecision(1) << std::setw(8) << v;
        }
        out << '\n';
    }

    InLine a = learnData[0];
    InLine b = learnData[1];
    size_t i = 0;
    size_t j = 1;
    size_t lcz = 2;
    double delta = 0.001;

    out << "Punkt A:";
    for(const auto& v : a.v) {
        out << std::setw(8) << std::fixed << std::setprecision(4) << v;
    }
    out << '\n';

    out << "Punkt B:";
    for(const auto& v : b.v) {
        out << std::setw(8) << std::fixed << std::setprecision(4) << v;
    }
    out << "\n\n";

    while (a.dist(b) > delta && lcz < lines && i < lines) {

        j++;
        if (j >= lines) {
            j %= lines;
            ++lcz;
        }

        out << "Punkt A:";
        for(const auto& v : a.v) {
            out << std::setw(8) << std::fixed << std::setprecision(4) << v;
        }
        out << '\n';

        out << "Punkt B:";
        for(const auto& v : b.v) {
            out << std::setw(8) << std::fixed << std::setprecision(4) << v;
        }
        out << '\n';
        out << "Obiekt nr";
        out << std::setw(4) << j;
        out << ";  Odleglosc pomiedzy A i B:";
        out << std::setw(14) << std::fixed << std::setprecision(6) << a.dist(b);
        out << "  lcz: " << lcz;
        out << "\n\n";

        if (learnData[j].c == c1 && (a - b) * (learnData[j] - a) < 0) {
            double t = ((learnData[j] - a) * (b - a)) / ((learnData[j] - a) * (learnData[j] - a));

            if (t < 1) {
                a = a + ((learnData[j] - a) * t);
            } else {
                a = learnData[j];
                ++i;
                lcz = 0;
                continue;
            }
        }

        if (learnData[j].c == c2 && (b - a) * (learnData[j] - b) < 0) {
            double t = ((learnData[j] - b) * (a - b)) / ((learnData[j] - b) * (learnData[j] - b));

            if (t < 1) {
                b = b + ((learnData[j] - b) * t);
            } else {
                b = learnData[j];
                ++i;
                lcz = 0;
                continue;
            }
        }

    }

    if (lcz != lines) {
        std::cout << "Zbiory X1 oraz X2 nie sa liniowo rozdzielne\n";
    } else {
        std::cout << " ZBIORY SA LINIOWO ROZDZIELNE\n\n";
        out << " ZBIORY SA LINIOWO ROZDZIELNE\n\n";
        std::cout << "Punkt A:";
        for(const auto& v : a.v) {
            std::cout << std::setw(8) << std::fixed << std::setprecision(4) << v;
        }
        std::cout << '\n';

        std::cout << "Punkt B:";
        for(const auto& v : b.v) {
            std::cout << std::setw(8) << std::fixed << std::setprecision(4) << v;
        }
        std::cout << '\n';

        out << " WERYFIKACJA\n\n";

        for(const auto& e : learnData) {
            out << " Klasa faktyczna:  " << e.c;
            out << " oraz przypisana:  ";
            if(e.dist(a) < e.dist(b)) {
                out << c1;
            } else {
                out << c2;
            }
            out << '\n';
        }

    }

    return 0;
}