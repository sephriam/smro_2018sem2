#include <p2c/p2c.h>

#define ncgl 10
#define ngl 10
#define mgl 10000

#define delta1 0.01
#define delta2 (-0.001)

typedef double txm[mgl];
typedef double *tx[ngl];
typedef long tlv[mgl];
typedef double tvs[ngl];

Static Char datname[13], resname[13];
Static FILE *dat, *res;
Static tx x, z;
Static long *lv, *lw;
Static double *mv, *sd, *a, *b, *c1, *c2;
Static long nc, n, m, m1, m2, min, kl1, kl2, k1, k2, k, ii, i, j, kl, lcz;
Static double ds, t;

Static Void AB(key) long key;
{
    long k, FORLIM;

    if (key == 1)
    {
        fprintf(res, "\n Punkt A: ");
        FORLIM = n;
        for (k = 0; k < FORLIM; k++)
            fprintf(res, "%8.4f", a[k]);
        fprintf(res, "\n Punkt B: ");
        FORLIM = n;
        for (k = 0; k < FORLIM; k++)
            fprintf(res, "%8.4f", b[k]);
        putc('\n', res);
        return;
    }
    printf("\n Punkt  A: ");
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        printf("%8.4f", a[k]);
    printf("\n Punkt  B: ");
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        printf("%8.4f", b[k]);
    putchar('\n');
}

Static Void test()
{
    long i, k, FORLIM, FORLIM1;

    ii = m1 + m2;
    FORLIM = ii;
    for (i = 0; i < FORLIM; i++)
    {
        fprintf(res, "%3ld", lw[i]);
        FORLIM1 = n;
        for (k = 0; k < FORLIM1; k++)
            fprintf(res, "%8.1f", z[k][i]);
        putc('\n', res);
    }
}

Static double dst()
{
    double ds = 0.0;
    long k, FORLIM;
    double TEMP;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
    {
        TEMP = a[k] - b[k];
        ds += TEMP * TEMP;
    }
    return ds;
}

Static double war1()
{
    double war = 0.0;
    long k, FORLIM;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        c1[k] = a[k] - b[k];
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        c2[k] = z[k][j - 1] - a[k];
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        war += c1[k] * c2[k];
    return war;
}

Static double war2()
{
    double war = 0.0;
    long k, FORLIM;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        c1[k] = b[k] - a[k];
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        c2[k] = z[k][j - 1] - b[k];
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        war += c1[k] * c2[k];
    return war;
}

Static double za2()
{
    double war = 0.0;
    long k, FORLIM;
    double TEMP;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        c2[k] = z[k][j - 1] - a[k];
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
    {
        TEMP = c2[k];
        war += TEMP * TEMP;
    }
    return war;
}

Static double zb2()
{
    double war = 0.0;
    long k, FORLIM;
    double TEMP;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
        c2[k] = z[k][j - 1] - b[k];
    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
    {
        TEMP = c2[k];
        war += TEMP * TEMP;
    }
    return war;
}

Static double dxa(i) long i;
{
    double war = 0.0;
    long k, FORLIM;
    double TEMP;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
    {
        TEMP = x[k][i - 1] - a[k];
        war += TEMP * TEMP;
    }
    return war;
}

Static double dxb(i) long i;
{
    double war = 0.0;
    long k, FORLIM;
    double TEMP;

    FORLIM = n;
    for (k = 0; k < FORLIM; k++)
    {
        TEMP = x[k][i - 1] - b[k];
        war += TEMP * TEMP;
    }
    return war;
}

main(argc, argv) int argc;
Char *argv[];
{
    long FORLIM;
    Char *TEMP;
    long FORLIM1;

    PASCAL_MAIN(argc, argv);
    res = NULL;
    dat = NULL;
    mv = (double *)Malloc(sizeof(tvs));
    sd = (double *)Malloc(sizeof(tvs));
    lv = (long *)Malloc(sizeof(tlv));
    for (j = 1; j <= ngl; j++)
        x[j - 1] = (double *)Malloc(sizeof(txm));
    lw = (long *)Malloc(sizeof(tlv));
    for (j = 1; j <= ngl; j++)
        z[j - 1] = (double *)Malloc(sizeof(txm));
    a = (double *)Malloc(sizeof(tvs));
    b = (double *)Malloc(sizeof(tvs));
    c1 = (double *)Malloc(sizeof(tvs));
    c2 = (double *)Malloc(sizeof(tvs));
    printf("\n  Nazwa pliku wejsciowego? ");
    fgets(datname, 13, stdin);
    TEMP = strchr(datname, '\n');
    if (TEMP != NULL)
        *TEMP = 0;
    printf("   Nazwa pliku wynikowego? ");
    fgets(resname, 13, stdin);
    TEMP = strchr(resname, '\n');
    if (TEMP != NULL)
        *TEMP = 0;

    dat = fopen(datname, "r");
    rewind(dat);
    fscanf(dat, "%ld%ld%ld%*[^\n]", &nc, &n, &m);
    getc(dat);
    FORLIM = m;
    for (i = 1; i <= FORLIM; i++)
    {
        fscanf(dat, "%ld", &lv[i - 1]);
        FORLIM1 = n;
        for (j = 1; j <= FORLIM1; j++)
            fscanf(dat, "%lg", &x[j - 1][i - 1]);
        fscanf(dat, "%*[^\n]");
        getc(dat);
    }
    if (dat != NULL)
        fclose(dat);
    dat = NULL;
    printf(" Numery rozdielanych klas? ");
    scanf("%ld%ld%*[^\n]", &kl1, &kl2);
    getchar();
    putchar('\n');
    m1 = 0;
    FORLIM = m;
    for (i = 1; i <= FORLIM; i++)
    {
        if (lv[i - 1] == kl1)
            m1++;
    }
    m2 = 0;
    FORLIM = m;
    for (i = 1; i <= FORLIM; i++)
    {
        if (lv[i - 1] == kl2)
            m2++;
    }
    min = m2;
    if (m1 < m2)
        min = m1;
    k1 = 0;
    k2 = 0;
    k = 0;
    FORLIM = m;
    for (ii = 1; ii <= FORLIM; ii++)
    {
        if (lv[ii - 1] == kl1 && k1 <= min)
        {
            k1++;
            i = (k1 - 1) * 2 + 1;
            lw[i - 1] = lv[ii - 1];
            FORLIM1 = n;
            for (j = 1; j <= FORLIM1; j++)
            {
                z[j - 1][i - 1] = x[j - 1][ii - 1];
            }
        }
        if (lv[ii - 1] == kl1 && k1 > min)
        {
            k++;
            i = min * 2 + k;
            lw[i - 1] = lv[ii - 1];
            FORLIM1 = n;
            for (j = 1; j <= FORLIM1; j++)
            {
                z[j - 1][i - 1] = x[j - 1][ii - 1];
            }
        }
        if (lv[ii - 1] == kl2 && k2 <= min)
        {
            k2++;
            i = k2 * 2;
            lw[i - 1] = lv[ii - 1];
            FORLIM1 = n;
            for (j = 1; j <= FORLIM1; j++)
            {
                z[j - 1][i - 1] = x[j - 1][ii - 1];
            }
        }
        if (lv[ii - 1] == kl2 && k2 > min)
        {
            k++;
            i = min * 2 + k;
            lw[i - 1] = lv[ii - 1];
            FORLIM1 = n;
            for (j = 1; j <= FORLIM1; j++)
            {
                z[j - 1][i - 1] = x[j - 1][ii - 1];
            }
        }
    }
    FORLIM = n;
    for (j = 1; j <= FORLIM; j++)
        a[j - 1] = z[j - 1][0];
    FORLIM = n;
    for (j = 1; j <= FORLIM; j++)
        b[j - 1] = z[j - 1][1];

    res = fopen(resname, "w+");
    if (res != NULL)
        rewind(res);
    else
        res = tmpfile();
    if (res == NULL)
        _EscIO(FileNotFound);
    fprintf(res, "\n Zbior uczacy uporzadkowany:\n");
    test();
    AB(1L);
    j = 2;
    lcz = 0;
    if (dst() > delta1)
    {
        do
        {
            if (j < m)
                j++;
            else
                j = 1;
            lcz++;
            if ((lw[j - 1] == kl1) & (war1() < delta2))
            {
                t = 0.0;
                lcz = 0;
                FORLIM = n;
                for (k = 1; k <= FORLIM; k++)
                    t += (z[k - 1][j - 1] - a[k - 1]) * (b[k - 1] - a[k - 1]) / za2();
                if (t < 1)
                {
                    FORLIM = n;
                    for (k = 1; k <= FORLIM; k++)
                    {
                        a[k - 1] += t * (z[k - 1][j - 1] - a[k - 1]);
                    }
                }
                else
                {
                    FORLIM = n;
                    for (k = 1; k <= FORLIM; k++)
                    {
                        a[k - 1] = z[k - 1][j - 1];
                    }
                }
            }
            if ((lw[j - 1] == kl2) & (war2() < delta2))
            {
                t = 0.0;
                lcz = 0;
                FORLIM = n;
                for (k = 1; k <= FORLIM; k++)
                    t += (z[k - 1][j - 1] - b[k - 1]) * (a[k - 1] - b[k - 1]) / zb2();
                if (t < 1)
                {
                    FORLIM = n;
                    for (k = 1; k <= FORLIM; k++)
                    {
                        b[k - 1] += t * (z[k - 1][j - 1] - b[k - 1]);
                    }
                }
                else
                {
                    FORLIM = n;
                    for (k = 1; k <= FORLIM; k++)
                    {
                        b[k - 1] = z[k - 1][j - 1];
                    }
                }
            }
            ds = dst();
            AB(1L);
            fprintf(res, " Obiekt nr %3ld;  Odleglosc pomiedzy A i B: %12.6f",
                    j, ds);
            fprintf(res, ";  lcz: %12ld\n", lcz);
            if (ds < delta1)
            {
                printf("\n ZBIORY NIE SA LINIOWO ROZDZIELNE\n");
                fprintf(res, "\n ZBIORY NIE SA LINIOWO ROZDZIELNE\n");
            }
            if (lcz == m1 + m2)
            {
                printf("\n ZBIORY SA LINIOWO ROZDZIELNE\n");
                fprintf(res, "\n ZBIORY SA LINIOWO ROZDZIELNE\n");
            }
        } while (ds >= delta1 && lcz != m1 + m2);
    }
    fprintf(res, "\n Weryfikacja:\n");
    FORLIM = m;
    for (i = 1; i <= FORLIM; i++)
    {
        if (lv[i - 1] == kl1 || lv[i - 1] == kl2)
        {
            kl = kl2;
            if (dxa(i) <= dxb(i))
                kl = kl1;
            fprintf(res, " Klasa faktyczna:%3ld oraz przypisana:%3ld\n",
                    lv[i - 1], kl);
        }
    }
    putc('\n', res);
    AB(0L);
    if (res != NULL)
        fclose(res);
    res = NULL;
    Free(mv);
    Free(sd);
    Free(lv);
    for (j = 1; j <= ngl; j++)
        Free(x[j - 1]);
    Free(lw);
    for (j = 1; j <= ngl; j++)
        Free(z[j - 1]);
    Free(a);
    Free(b);
    Free(c1);
    Free(c2);
    if (dat != NULL)
        fclose(dat);
    exit(EXIT_SUCCESS);
}
