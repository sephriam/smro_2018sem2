#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <iomanip>

struct In {
    unsigned int cls;
    std::vector<double> features;
};

int main(int argc, char* argv[]) {

    if(argc < 3) {
        std::cout << argv[0] << " " << "<zbior uczacy> <zbior testowy>" << std::endl;
        return 0;
    }

    std::cout << std::setw(6);

    std::ifstream learnDS(argv[1]);
    size_t classes = 0;
    size_t features = 0;
    size_t cases = 0;

    learnDS >> classes >> features >> cases;

    std::vector<In> lines;
    std::vector<double> meanVals(features, 0.0);

    for(size_t i = 0; i < cases; ++i) {
        In reader;
        learnDS >> reader.cls;
        for(size_t j = 0; j < features; ++j) {
            double val;
            learnDS >> val;
            reader.features.push_back(val);
            meanVals[j] += val;
        }
        lines.push_back(reader);
    }

    for(size_t i = 0; i < features; ++i) {
        meanVals[i] /= cases;
    }

    std::vector<double> stdDev(features, 0.0);

    for(size_t i = 0; i < cases; ++i) {
        for(size_t j = 0; j < features; ++j) {
            stdDev[j] += ((lines[i].features[j] - meanVals[j]) * (lines[i].features[j] - meanVals[j])) / (cases - 1);
        }
    }

    for(size_t i = 0; i < features; ++i) {
        stdDev[i] = std::sqrt(stdDev[i]);
    }

    std::vector<std::vector<double>> centers(classes, std::vector<double>(features, 0.0));
    std::vector<std::vector<size_t>> amount(classes, std::vector<size_t>(features, 0));

    for(const auto& l : lines) {
        for(size_t i = 0; i < features; ++i) {
            centers[l.cls - 1][i] += l.features[i];
            ++amount[l.cls - 1][i];
        }
    }

    std::cout << "\nSrodki ciezkosci klas przed standaryzacja:\n";

    for(size_t i = 0; i < classes; ++i) {
        for(size_t j = 0; j < features; ++j) {
            centers[i][j] /= amount[i][j];
            std::cout << std::setw(8) << std::setprecision(4) << std::fixed << centers[i][j];
            centers[i][j] = (centers[i][j] - meanVals[j]) / stdDev[j];
        }
        std::cout << '\n';
    }

    std::cout << "\nSrodki ciezkosci klas po standaryzacji:\n";

    for(size_t i = 0; i < classes; ++i) {
        for(size_t j = 0; j < features; ++j) {
            std::cout << std::setw(8) << std::setprecision(4) << std::fixed << centers[i][j];
        }
        std::cout << '\n';
    }

    std::cout << "\nWyniki testu:\nNr obj, Klasa faktyczna, Klasa przypisana\n";

    std::ifstream testDS(argv[2]);
    testDS >> classes >> features >> cases;
    size_t incorrect = 0;

    for(size_t i = 0; i < cases; ++i) {
        In reader;
        std::vector<double> dist(classes);
        testDS >> reader.cls;
        for(size_t j = 0; j < features; ++j) {
            double val;
            testDS >> val;
            val = (val - meanVals[j]) / stdDev[j];
            reader.features.push_back(val);
        }

        for(size_t j = 0; j < classes; ++j) {
            dist[j] = 0.0;
            for(size_t k = 0; k < features; ++k) {
                dist[j] += (reader.features[k] - centers[j][k]) * (reader.features[k] - centers[j][k]);
            }
            dist[j] /= features;
        }

        size_t classifiedClass = classes;

        for(size_t j = 0; j < classes; ++j) {
            if(dist[j] < dist[classifiedClass - 1]) {
                classifiedClass = j + 1;
            }
        }

        std::cout << std::setw(6) << std::setprecision(4) << std::fixed << i + 1;
        std::cout << std::setw(16) << std::setprecision(4) << std::fixed << reader.cls;
        std::cout << std::setw(18) << std::setprecision(4) << std::fixed << classifiedClass << '\n';
        if(classifiedClass != reader.cls) {
            ++incorrect;
        }
    }

    std::cout << "Odsetek mylnych decyzji:  " << std::setprecision(1) << 100.0 * double(incorrect) / double(cases) << "%\n";


    return 0;
}